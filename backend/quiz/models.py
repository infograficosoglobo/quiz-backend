from django.db import models
import os
from uuid import uuid4
from django.utils.deconstruct import deconstructible
from django.utils.text import slugify
from django.conf import settings

@deconstructible
class UploadToPathAndRename(object):

    def __init__(self, path):
        self.sub_path = path

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        # get filename
        if instance.pk:
            filename = '{}.{}'.format(instance.pk, ext)
        else:
            # set filename as random string
            filename = '{}.{}'.format(uuid4().hex, ext)
        # return the whole path to the file
        z = self.sub_path
        newpath = z.replace('target', slugify(instance.title))
        return os.path.join(newpath, filename)


class Quiz(models.Model):
    title = models.CharField(max_length=500, null=False, blank=False)
    description = models.CharField(max_length=500, null=True)
    isActive = models.BooleanField(default=True)
    quizImg = models.FileField(
        max_length=1000, blank=True,
        upload_to=UploadToPathAndRename(
            os.path.join(settings.MEDIA_ROOT, 'quiz', 'target')))
    
    def __unicode__(self):
        return self.question_set.name