## Configurando ambiente ##

#### Clonar repositório do projeto ####

```bash

git clone https://wxjunior@bitbucket.org/infograficosoglobo/quiz-backend.git

cd quiz-backend

```

#### Acessar o ambiente virtual:

O ambiente virtual se encontra na raíz do projeto, pasta "env"

Para acessá-lo, rode o seguinte comando na raíz do projeto:

```bash

source env/bin/activate

```

#### Instalar requerimentos:

```bash

pip3 install -r requirements.txt 

```
#### Criando banco MySql:

No terminal mysql:

```bash

CREATE DATABASE homestead; 

```
#### Rodando migrations

```
cd backend
python3 manage.py makemigrations
python3 manage.py migrate
```

#### Carregando dump do banco de dados

```
python3 manage.py loaddata database_dump.json
```

#### Criando Superuser e utilizando admin

```
python3 manage.py createsuperuser
```

Para criar as entidades para o projeto
Primeiramente deve ser criado o Quiz, em seguida a Question referenciando o ID do quiz e por último as Choices referenciando o ID da Question

#### Rodando o servidor

```bash
python3 manage.py runserver

```
A aplicação rodará em: http://localhost:8000/


## Rotas
A api servirá um json a fim de alimentar o frontend em React sempre que for chamada.
A idéia é servir um quiz a cada requisição para que o React sirva a aplicação final.

### Quiz

* Rota para servir o quiz, pode ser executada no navegador ex.: http://localhost:8000/quiz/1

Método | URI | Params | Descrição | Ex.:
--- | --- | --- | --- | ---
*GET* | `http://localhost:8000/quiz/{{quiz_id}}` | **quiz_id** | Retorna quiz por id | `http://localhost:8000/quiz/1`


## Features

-  [MySql](https://www.postgresql.org/)

-  [Django](https://www.djangoproject.com/)

## Requerimentos
-  [Python3](https://www.python.org/downloads/)

## Suporte
  
Desenvolvido por wellington.junior@oglobo.com.br - @Infoglobo)